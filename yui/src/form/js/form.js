/**
 * JavaScript for form editing group conditions.
 *
 * @module moodle-availability_groupsubstring-form
 */
M.availability_groupsubstring = M.availability_groupsubstring || {};

/**
 * @class M.availability_groupsubstring.form
 * @extends M.core_availability.plugin
 */
M.availability_groupsubstring.form = Y.Object(M.core_availability.plugin);

/**
 * Groups available for selection (alphabetical order).
 *
 * @property groups
 * @type Array
 */
M.availability_groupsubstring.form.groups = null;

/**
 * Initialises this plugin.
 *
 * @method initInner
 * @param {Array} groups Array of objects containing groupid => name
 */
M.availability_groupsubstring.form.initInner = function (groups) {
    this.groups = groups;
};

M.availability_groupsubstring.form.getNode = function (json) {
    // Create HTML structure.
    var html = '<label><span class="pr-3">' + M.util.get_string('title', 'availability_groupsubstring') + '</span> ' +
        '<span class="availability-group">' +
        '<input name="id" class="custom-input">' +
        '</span></label>';
    var node = Y.Node.create('<span class="form-inline">' + html + '</span>');

    // Set initial values (leave default 'choose' if creating afresh).
    if (json.creating === undefined) {
        if (json.id !== undefined) {
            node.one('input[name=id]').set('value', '' + json.id);
        } else if (json.id === undefined) {
            node.one('select[name=id]').set('value', '');
        }
    }

    // Add event handlers (first time only).
    if (!M.availability_groupsubstring.form.addedEvents) {
        M.availability_groupsubstring.form.addedEvents = true;
        var root = Y.one('.availability-field');
        root.delegate('change', function () {
            // Just update the form fields.
            M.core_availability.form.update();
        }, '.availability_groupsubstring input');
    }

    return node;
};

M.availability_groupsubstring.form.fillValue = function (value, node) {
    var selected = node.one('input[name=id]').get('value');
    if (selected !== '') {
        value.id = selected;
    }
};

M.availability_groupsubstring.form.fillErrors = function (errors, node) {
    var value = {};
    this.fillValue(value, node);
};
